This is a list of all LSDalton related branches.

We need to have a clear picture at which branches should be:

1. deleted
2. saved in LSDalton repo
3. saved in Dalton Repo.

therefore please update this list when you make a new branch with the intention of modifying LSDalton code.

The Status can be :

Stale          Will be deleted once everybody agrees that the branch is stale
Working        Will be saved in the LSDalton repo
DoNotDelete    Will be saved in the LSDalton repo (maybe as read only)

List of branches that people have taken responsibility for.

chandan_nmr                     Author: Thomas,Chandan          Status: Working
dmp2opt                         Author: Johannes                Status: Working
titan_branch                    Author: Patrick E., Janus       Status: Working
qcmatrix                        Author: Bin Gao, Magnus, ..     Status: Working (new OpenRSP interface with QcMatrix library)
idamarie/nonortho               Author: Ida-Marie Høyvik        Status: Working
unres                           Author: Dmytro Bykov            Status: Working
ThomasK/THC                     Author: Thomas                  Status: Working
sarai/dmcc                      Author: Sarai Folkestad         Status: Working
karen/hartreefock               Author: Karen Dundas            Status: Working

pe/lsdalton                     Author: Magnus,Nanna,Simen      Status: DoNotDelete

dec-karl                        Author: Karl                    Status: stale
localize_wannier                Author: Karl                    Status: stale
dmcc                            Author: Ida-Marie Høyvik        Status: stale
wannier                         Author: Karl                    Status: Working


The following branches will not move. They are basically tags
to very old code:
radovan/linsca-openrsp-2010     Maintainer: Radovan             Status: DoNotDelete (might be used to scavenge old OpenRSP test reference results, does not need to move to LSDalton repo)
radovan/linsca-vcd-aat          Author: Radovan                 Status: DoNotDelete (might be used to reproduce reference numbers of VCD AAT implementation)
LstensorRemovalBranch           Author: Thomas, Simen           Status: DoNotDelete
linsca-develop                  Author: Thomas                  Status: DoNotDelete (Old LSDalton Master branch)
linsca-develop-xcfun            Author: Ulf                     Status: DoNotDelete
linsca-ng                       Author: Thomas,Sonia,..         Status: DoNotDelete (Only place Residues are implemented,..)
linsca-release                  Author: Thomas,Simen            Status: DoNotDelete (2011 release of lsdalton)
dec-cholesky                    Author: Pablo                   Status: DoNotDelete
dec-quad                        Author: Johannes                Status: DoNotDelete
ichor-improvements              Author: Thomas                  Status: DoNotDelete
NOorbitals                      Author: Ida-Marie Høyvik        Status: DoNotDelete
NonOrthogonalOrbitals           Author: Ida-Marie Høyvik        Status: DoNotDelete
master_with_CUDA_fh             Author: Frank Heiden haydn@bbsyd.dk  Status: ??????????????????????

